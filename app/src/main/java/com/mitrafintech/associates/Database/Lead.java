package com.mitrafintech.associates.Database;

public class Lead {
    String id;
    String name;
    String mobile;
    String product;
    Integer income;
    Integer age;
    String company;
    String bank;
    String submittedby;
    Integer status;
    String createdon;
    String changedon;
    String appliedProduct;
    String pincode;
    String address;

    public Lead() {
    }

    public Lead(String id, String name, String mobile, String product, Integer income, Integer age, String company, String bank, String submittedby, Integer status, String createdon, String changedon, String appliedProduct, String pincode, String address) {
        this.id = id;
        this.name = name;
        this.mobile = mobile;
        this.product = product;
        this.income = income;
        this.age = age;
        this.company = company;
        this.bank = bank;
        this.submittedby = submittedby;
        this.status = status;
        this.createdon = createdon;
        this.changedon = changedon;
        this.appliedProduct = appliedProduct;
        this.pincode = pincode;
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Integer getIncome() {
        return income;
    }

    public void setIncome(Integer income) {
        this.income = income;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getSubmittedby() {
        return submittedby;
    }

    public void setSubmittedby(String submittedby) {
        this.submittedby = submittedby;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreatedon() {
        return createdon;
    }

    public void setCreatedon(String createdon) {
        this.createdon = createdon;
    }

    public String getChangedon() {
        return changedon;
    }

    public void setChangedon(String changedon) {
        this.changedon = changedon;
    }

    public String getAppliedProduct() {
        return appliedProduct;
    }

    public void setAppliedProduct(String appliedProduct) {
        this.appliedProduct = appliedProduct;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
