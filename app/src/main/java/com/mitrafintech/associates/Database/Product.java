package com.mitrafintech.associates.Database;

public class Product {
    String id;
    String image;
    String bankImage;
    String product;
    String name;
    String bank;
    String fees;
    String description;
    String category;
    Integer income;
    Integer hotincome;
    Integer rating;

    public Product() {
    }

    public Product(String id, String image, String bankImage, String product, String name, String bank, String fees, String description, String category, Integer income, Integer hotincome, Integer rating) {
        this.id = id;
        this.image = image;
        this.bankImage = bankImage;
        this.product = product;
        this.name = name;
        this.bank = bank;
        this.fees = fees;
        this.description = description;
        this.category = category;
        this.income = income;
        this.hotincome = hotincome;
        this.rating = rating;
    }

    public String getBankImage() {
        return bankImage;
    }

    public void setBankImage(String bankImage) {
        this.bankImage = bankImage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getIncome() {
        return income;
    }

    public void setIncome(Integer income) {
        this.income = income;
    }

    public Integer getHotincome() {
        return hotincome;
    }

    public void setHotincome(Integer hotincome) {
        this.hotincome = hotincome;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}
