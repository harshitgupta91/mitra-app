package com.mitrafintech.associates.Database;

public class User {
    String id;
    String name;
    String mobile;
    String email;
    String occupation;
    String income;
    boolean isExperienced;
    String createdon;
    Integer userType;
    String image;
    boolean mobileVerified;



    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public boolean isExperienced() {
        return isExperienced;
    }

    public void setExperienced(boolean experienced) {
        isExperienced = experienced;
    }

    public String getCreatedon() {
        return createdon;
    }

    public void setCreatedon(String createdon) {
        this.createdon = createdon;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isMobileVerified() {
        return mobileVerified;
    }

    public void setMobileVerified(boolean mobileVerified) {
        this.mobileVerified = mobileVerified;
    }

    public User(String id, String name, String mobile, String email, String occupation, String income, boolean isExperienced, String createdon, Integer userType, String image, boolean mobileVerified) {
        this.id = id;
        this.name = name;
        this.mobile = mobile;
        this.email = email;
        this.occupation = occupation;
        this.income = income;
        this.isExperienced = isExperienced;
        this.createdon = createdon;
        this.userType = userType;
        this.image = image;
        this.mobileVerified = mobileVerified;
    }
}
