package com.mitrafintech.associates.Fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.chip.Chip;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mitrafintech.associates.Application.Constant;
import com.mitrafintech.associates.Application.Helper;
import com.mitrafintech.associates.Application.LeadStatusStep;
import com.mitrafintech.associates.Database.Lead;
import com.mitrafintech.associates.Database.Product;
import com.mitrafintech.associates.Database.User;
import com.mitrafintech.associates.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import ernestoyaquello.com.verticalstepperform.VerticalStepperFormView;
import ernestoyaquello.com.verticalstepperform.listener.StepperFormListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class LeadsFragment extends Fragment {


    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private ProgressDialog progressDialog;

    public LeadsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_leads, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        firebaseDatabase = Helper.getDatabase();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = firebaseDatabase.getReference(Constant.FIREBASE_DB);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        mDatabase.child("user").child(mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                User user = dataSnapshot.getValue(User.class);
                if ((user != null ? user.getUserType() : 0) == 0) {
                    setupRecyclerView(view, false);
                } else {
                    setupRecyclerView(view, true);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void setupRecyclerView(View view, boolean showAll) {
        RecyclerView leadsRecyclerView = view.findViewById(R.id.leads);
        final List<Lead> leadList = new ArrayList<>();
        final LeadsAdapter leadsAdapter = new LeadsAdapter(leadList, showAll);

        leadsRecyclerView.setAdapter(leadsAdapter);
        leadsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        Query query;
        if (showAll) {
            query = mDatabase.child("lead").orderByChild("createdon");
        } else {
            query = mDatabase.child("lead").orderByChild("submittedby").equalTo(mAuth.getCurrentUser().getUid());
        }

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progressDialog.dismiss();
                leadList.clear();
                for (DataSnapshot snapshot :
                        dataSnapshot.getChildren()) {
                    Lead lead = snapshot.getValue(Lead.class);
                    leadList.add(lead);
                }
                if (leadList.size() == 0) {
                    Toast.makeText(getActivity(), "NO LEADS FOUND", Toast.LENGTH_LONG).show();
                }
                Collections.reverse(leadList);
                leadsAdapter.updateList(leadList);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void ShowLeadBottomSheet(Lead lead) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getActivity());
        View sheetView = getActivity().getLayoutInflater().inflate(R.layout.timeline_dialog, null);

        // Create the steps.
        LeadStatusStep step1, step2, step3, step4, step5;

        if (lead.getStatus() >= 1) {
            step1 = new LeadStatusStep(Helper.getStatusString(1), "", getActivity());
            step1.markAsCompleted(false);
        } else {
            step1 = new LeadStatusStep(Helper.getStatusString(1), "ACTIVE", getActivity());
        }

        if (lead.getStatus() >= 2) {
            step2 = new LeadStatusStep(Helper.getStatusString(2), "", getActivity());
            step2.markAsCompleted(false);
        } else {
            step2 = new LeadStatusStep(Helper.getStatusString(2), "ACTIVE", getActivity());
        }

        if (lead.getStatus() >= 3) {
            step3 = new LeadStatusStep(Helper.getStatusString(3), "", getActivity());
            step3.markAsCompleted(false);
        } else {
            step3 = new LeadStatusStep(Helper.getStatusString(3), "ACTIVE", getActivity());
        }

        if (lead.getStatus() >= 4) {
            step4 = new LeadStatusStep(Helper.getStatusString(4), "", getActivity());
            step4.markAsCompleted(false);
        } else {
            step4 = new LeadStatusStep(Helper.getStatusString(4), "ACTIVE", getActivity());
        }

        if (lead.getStatus() >= 5) {
            step5 = new LeadStatusStep(Helper.getStatusString(5), "", getActivity());
            step5.markAsCompleted(false);
        } else {
            step5 = new LeadStatusStep(Helper.getStatusString(5), "ACTIVE", getActivity());
        }


        // Find the form view, set it up and initialize it.
        VerticalStepperFormView verticalStepperForm = sheetView.findViewById(R.id.stepper_form);
        verticalStepperForm
                .setup(new StepperFormListener() {
                    @Override
                    public void onCompletedForm() {

                    }

                    @Override
                    public void onCancelledForm() {

                    }
                }, step1, step2, step3, step4, step5)
                .init();
        verticalStepperForm.goToStep(lead.getStatus(), true);
        verticalStepperForm.hideBottomNavigation();
        bottomSheetDialog.setContentView(sheetView);
        bottomSheetDialog.show();
    }

    public class LeadsAdapter extends RecyclerView.Adapter<LeadsAdapter.ViewHolder> {

        private List<Lead> leadList;
        private boolean showAll;

        public LeadsAdapter(List<Lead> leadList, boolean showAll) {
            this.leadList = leadList;
            this.showAll = showAll;
        }

        @NonNull
        @Override
        public LeadsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View contactView = inflater.inflate(R.layout.list_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(contactView);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull final LeadsAdapter.ViewHolder holder, int position) {
            final Lead lead = leadList.get(position);
            holder.tvProduct.setText(lead.getProduct());
            if (!lead.getAppliedProduct().equals("")) {
                mDatabase.child("product").child(lead.getAppliedProduct()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Product product = dataSnapshot.getValue(Product.class);
                        holder.tvApplied.setText(product != null ? product.getName() : "");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            holder.tvName.setText(lead.getName());
            DateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy");
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

            try {
                dateFormat.parse(lead.getCreatedon());
                holder.tvCreatedOn.setText(dateFormat1.format(dateFormat.parse(lead.getCreatedon())));

            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.tvEmail.setText(lead.getCompany());
            holder.chipStatus.setText(Helper.getStatusString(lead.getStatus()));
            holder.chipCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String phone = lead.getMobile();
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                    startActivity(intent);
                }
            });

            if (showAll && lead.getStatus() < 5) {
                holder.btnStatusChange.setVisibility(View.VISIBLE);
                holder.btnStatusChange.setText(Helper.getStatusString(lead.getStatus() + 1));
                holder.btnStatusChange.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("status", lead.getStatus() + 1);
                        mDatabase.child("lead").child(lead.getId()).updateChildren(map);
                    }
                });
            } else {
                holder.btnStatusChange.setVisibility(View.GONE);
            }

            holder.btnExpand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ShowLeadBottomSheet(lead);
                }
            });
        }

        @Override
        public int getItemCount() {
            return leadList.size();
        }

        public void updateList(List<Lead> leadList) {
            this.leadList = leadList;
            notifyDataSetChanged();
        }


        private class ViewHolder extends RecyclerView.ViewHolder {

            TextView tvProduct, tvApplied, tvName, tvCreatedOn, tvEmail;
            Chip chipStatus, chipCall;
            MaterialButton btnStatusChange;
            ImageButton btnExpand;

            public ViewHolder(View itemView) {
                super(itemView);
                tvProduct = itemView.findViewById(R.id.product);
                tvApplied = itemView.findViewById(R.id.applied);
                tvName = itemView.findViewById(R.id.name);
                tvEmail = itemView.findViewById(R.id.email);
                tvCreatedOn = itemView.findViewById(R.id.createdon);
                chipStatus = itemView.findViewById(R.id.status);
                chipCall = itemView.findViewById(R.id.call);
                btnStatusChange = itemView.findViewById(R.id.change_status);
                btnExpand = itemView.findViewById(R.id.expand);
            }
        }
    }

}
