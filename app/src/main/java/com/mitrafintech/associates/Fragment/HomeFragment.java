package com.mitrafintech.associates.Fragment;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.chip.Chip;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mitrafintech.associates.Application.Constant;
import com.mitrafintech.associates.Application.Helper;
import com.mitrafintech.associates.Database.User;
import com.mitrafintech.associates.R;
import com.smarteist.autoimageslider.SliderView;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HomeFragment extends Fragment {


    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private MyAdapter myAdapter;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        firebaseDatabase = Helper.getDatabase();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = firebaseDatabase.getReference(Constant.FIREBASE_DB);

        initAccountChip(view);
        initLeaderBoard(view);
        initImageSlider(view);
    }

    private void initImageSlider(View view) {
        SliderView sliderView = view.findViewById(R.id.imageSlider);
        SliderAdapterExample adapter = new SliderAdapterExample(getActivity());
        sliderView.setSliderAdapter(adapter);
        sliderView.startAutoCycle();
    }

    private void initLeaderBoard(View view) {
        ListView listView = view.findViewById(R.id.leaders);
        final List<User> userList = new ArrayList<>();
        myAdapter = new MyAdapter(userList);
        listView.setAdapter(myAdapter);
        mDatabase.child("user").orderByChild("income").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userList.clear();
                for (DataSnapshot snapshot :
                        dataSnapshot.getChildren()) {
                    userList.add(snapshot.getValue(User.class));
                }
                Collections.reverse(userList);
                myAdapter.update(userList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void initAccountChip(View view) {
        final Chip chip = view.findViewById(R.id.account);
        chip.setText(mAuth.getCurrentUser().getDisplayName());
        Glide.with(getActivity())
                .asDrawable()
                .load(mAuth.getCurrentUser().getPhotoUrl())
                .apply(RequestOptions.circleCropTransform())
                .into(new CustomTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        chip.setChipIcon(resource);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {

                    }
                });
    }

    private class MyAdapter extends BaseAdapter {

        private List<User> users;

        public MyAdapter(List<User> users) {
            this.users = users;
        }

        @Override
        public int getCount() {
            return users.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (convertView == null) {
                view = getActivity().getLayoutInflater().inflate(R.layout.leaders_item, null);
            }
            User user = users.get(position);

            ImageView imageView = view.findViewById(R.id.badge);
            imageView.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.Grey500)));

            if (position == 0) {
                imageView.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.Green500)));
            }

            if (position == 1) {
                imageView.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.Blue500)));
            }

            if (position == 2) {
                imageView.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.Amber500)));
            }

            ImageView imageView1 = view.findViewById(R.id.image);
            Glide.with(getActivity()).load(user.getImage()).apply(RequestOptions.circleCropTransform()).into(imageView1);
            TextView textView = view.findViewById(R.id.name);
            textView.setText(user.getName());

            Chip chip = view.findViewById(R.id.amount);
            chip.setText(user.getIncome());

            return view;
        }

        public void update(List<User> users) {
            this.users = users;
            notifyDataSetChanged();
        }
    }

    public class SliderAdapterExample extends SliderViewAdapter<SliderAdapterExample.SliderAdapterVH> {

        private Context context;

        public SliderAdapterExample(Context context) {
            this.context = context;
        }

        @Override
        public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
            View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, null);
            return new SliderAdapterVH(inflate);
        }

        @Override
        public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {
            viewHolder.textViewDescription.setText("");

            switch (position) {
                case 0:
                    Glide.with(viewHolder.itemView)
                            .load(R.drawable.a1)
                            .into(viewHolder.imageViewBackground);
                    break;
                case 1:
                    Glide.with(viewHolder.itemView)
                            .load(R.drawable.a2)
                            .into(viewHolder.imageViewBackground);
                    break;
                default:
                    Glide.with(viewHolder.itemView)
                            .load(R.drawable.a3)
                            .into(viewHolder.imageViewBackground);
                    break;

            }

        }

        @Override
        public int getCount() {
            //slider view count could be dynamic size
            return 3;
        }

        class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

            View itemView;
            ImageView imageViewBackground;
            TextView textViewDescription;

            public SliderAdapterVH(View itemView) {
                super(itemView);
                imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
                textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider);
                this.itemView = itemView;
            }
        }
    }

}
