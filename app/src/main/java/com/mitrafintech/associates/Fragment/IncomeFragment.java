package com.mitrafintech.associates.Fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mitrafintech.associates.Application.Constant;
import com.mitrafintech.associates.Application.Helper;
import com.mitrafintech.associates.Database.Lead;
import com.mitrafintech.associates.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class IncomeFragment extends Fragment {


    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    public IncomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_income, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        firebaseDatabase = Helper.getDatabase();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = firebaseDatabase.getReference(Constant.FIREBASE_DB);

        mDatabase.child("lead").orderByChild("submittedby").equalTo(mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<Lead> leadList = new ArrayList<>();
                for (DataSnapshot snapshot :
                        dataSnapshot.getChildren()) {
                    Lead lead = snapshot.getValue(Lead.class);
                    leadList.add(lead);
                }
                CalculateAndSetIncome(view, leadList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void CalculateAndSetIncome(View view, List<Lead> leadList) {
        Integer totalIncome = 0;
        Integer incomeVerfication = 0;
        Integer incomeCollection = 0;
        Integer incomeSent = 0;

        for (Lead lead : leadList) {
            if (lead.getStatus() == 5) {
                incomeSent += 500;
            }
            if (lead.getStatus() >= 2) {
                incomeVerfication += 100;
            }
            if (lead.getStatus() >= 3) {
                incomeCollection += 100;
            }
        }

        totalIncome = incomeCollection + incomeSent + incomeVerfication;
        TextView tI, iC, iS, iV;
        tI = view.findViewById(R.id.ti);
        iC = view.findViewById(R.id.ic);
        iS = view.findViewById(R.id.is);
        iV = view.findViewById(R.id.iv);

        tI.setText("Rs " + totalIncome);
        iC.setText("Rs " + incomeCollection);
        iV.setText("Rs " + incomeVerfication);
        iS.setText("Rs " + incomeSent);

    }

}
