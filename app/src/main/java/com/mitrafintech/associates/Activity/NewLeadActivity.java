package com.mitrafintech.associates.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mitrafintech.associates.Application.Constant;
import com.mitrafintech.associates.Application.Helper;
import com.mitrafintech.associates.Database.Lead;
import com.mitrafintech.associates.R;

import java.util.Arrays;

public class NewLeadActivity extends AppCompatActivity {

    private FirebaseAnalytics firebaseAnalytics;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private String[] product = {"Credit card"};
    private String[] bank = {"RBL BANK", "YES BANK", "INUSIND BANK", "HDFC BANK"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_lead);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        firebaseDatabase = Helper.getDatabase();
        mDatabase = firebaseDatabase.getReference(Constant.FIREBASE_DB);
        mAuth = FirebaseAuth.getInstance();

        initToolbar("New Lead");
        initProductSpinner();
        initBankSpinner();
        submitLead();
    }

    private void submitLead() {

        findViewById(R.id.submitlead).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.hideKeyboard(NewLeadActivity.this);
                boolean isValid = true;
                if (((TextInputEditText) findViewById(R.id.name)).getText().length() == 0) {
                    ((TextInputLayout) findViewById(R.id.name_box)).setError("Required");
                    isValid = false;
                } else {
                    ((TextInputLayout) findViewById(R.id.name_box)).setErrorEnabled(false);
                }

                if (((TextInputEditText) findViewById(R.id.mobile)).getText().length() == 0) {
                    ((TextInputLayout) findViewById(R.id.mobile_box)).setError("Required");
                    isValid = false;
                } else {
                    ((TextInputLayout) findViewById(R.id.mobile_box)).setErrorEnabled(false);
                }

                if (((TextInputEditText) findViewById(R.id.income)).getText().length() == 0) {
                    ((TextInputLayout) findViewById(R.id.income_box)).setError("Required");
                    isValid = false;
                } else {
                    ((TextInputLayout) findViewById(R.id.income_box)).setErrorEnabled(false);
                }

                if (((TextInputEditText) findViewById(R.id.age)).getText().length() == 0) {
                    ((TextInputLayout) findViewById(R.id.age_box)).setError("Required");
                    isValid = false;
                } else {
                    ((TextInputLayout) findViewById(R.id.age_box)).setErrorEnabled(false);
                }

                if (((TextInputEditText) findViewById(R.id.company)).getText().length() == 0) {
                    ((TextInputLayout) findViewById(R.id.company_box)).setError("Required");
                    isValid = false;
                } else {
                    ((TextInputLayout) findViewById(R.id.company_box)).setErrorEnabled(false);
                }

                if (((TextInputEditText) findViewById(R.id.pincode)).getText().length() == 0) {

                    ((TextInputLayout) findViewById(R.id.pincode_box)).setError("Required");
                    isValid = false;
                } else if (!((TextInputEditText) findViewById(R.id.pincode)).getText().toString().startsWith("4")) {
                    ((TextInputLayout) findViewById(R.id.pincode_box)).setError("We only have services in Maharashtra");
                    isValid = false;
                } else {
                    ((TextInputLayout) findViewById(R.id.pincode_box)).setErrorEnabled(false);
                }

                if (isValid) {

                    Spinner spinner = findViewById(R.id.product);
                    String product = (String) spinner.getSelectedItem();

                    Spinner spinner2 = findViewById(R.id.bank);
                    String bank = (String) spinner2.getSelectedItem();

                    String id = mDatabase.child("lead").push().getKey();
                    final Lead lead = new Lead(id,
                            ((TextInputEditText) findViewById(R.id.name)).getText().toString(),
                            ((TextInputEditText) findViewById(R.id.mobile)).getText().toString(),
                            product,
                            Integer.parseInt(((TextInputEditText) findViewById(R.id.income)).getText().toString()),
                            Integer.parseInt(((TextInputEditText) findViewById(R.id.age)).getText().toString()),
                            ((TextInputEditText) findViewById(R.id.company)).getText().toString(),
                            bank,
                            mAuth.getCurrentUser().getUid(),
                            1,
                            Helper.currentDateTime(),
                            Helper.currentDateTime(),
                            "",
                            ((TextInputEditText) findViewById(R.id.pincode)).getText().toString(),
                            ((TextInputEditText) findViewById(R.id.address)).getText().toString()
                    );

                    mDatabase.child("lead").child(id).setValue(lead).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                findViewById(R.id.submitted).setVisibility(View.VISIBLE);
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent = new Intent(NewLeadActivity.this, ResultsActivity.class);
                                        intent.putExtra("leadid", lead.getId());
                                        startActivity(intent);
                                        finish();
                                    }
                                }, 5000);
                            }
                        }
                    });

                }

            }
        });


    }

    private void initBankSpinner() {
        Spinner spinner = findViewById(R.id.bank);
        ArrayAdapter adapter = new ArrayAdapter<>(this, R.layout.spinner_item, Arrays.asList(bank));
        spinner.setAdapter(adapter);
    }

    private void initProductSpinner() {
        Spinner spinner = findViewById(R.id.product);
        ArrayAdapter adapter = new ArrayAdapter<>(this, R.layout.spinner_item, Arrays.asList(product));
        spinner.setAdapter(adapter);
    }

    private void initToolbar(String register) {
        ((TextView) findViewById(R.id.title)).setText(register);
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
