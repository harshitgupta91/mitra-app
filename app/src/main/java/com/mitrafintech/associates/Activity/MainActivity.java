package com.mitrafintech.associates.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.mitrafintech.associates.Fragment.HomeFragment;
import com.mitrafintech.associates.Fragment.IncomeFragment;
import com.mitrafintech.associates.Fragment.LeadsFragment;
import com.mitrafintech.associates.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setFloatingButton();
        setBottomNavigation();


    }

    private void setFloatingButton() {
        findViewById(R.id.newlead).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NewLeadActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setBottomNavigation() {
        getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, new HomeFragment(), "HomeFragment").commit();
        BottomNavigationView navView = findViewById(R.id.bottom_navigation);
        navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.navigation_home: {
                        getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, new HomeFragment(), "HomeFragment").commit();
                        break;
                    }
                    case R.id.navigation_income: {
                        getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, new IncomeFragment(), "IncomeFragment").commit();
                        break;
                    }
                    case R.id.navigation_lead: {
                        getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, new LeadsFragment(), "LeadsFragment").commit();
                        break;
                    }
                }
                return true;
            }
        });
    }


}
