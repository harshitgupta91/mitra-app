package com.mitrafintech.associates.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.FirebaseException;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mitrafintech.associates.Application.Constant;
import com.mitrafintech.associates.Application.Helper;
import com.mitrafintech.associates.Database.User;
import com.mitrafintech.associates.R;

import java.util.concurrent.TimeUnit;

public class RegisterActivity extends AppCompatActivity {

    private FirebaseAnalytics firebaseAnalytics;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private ProgressDialog progressDialog;
    private boolean mobileVerified;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        firebaseDatabase = Helper.getDatabase();
        mDatabase = firebaseDatabase.getReference(Constant.FIREBASE_DB);
        mAuth = FirebaseAuth.getInstance();

        initToolbar("Register");
        preFillForm();
        register();
        verifyMobile();
    }

    private void verifyMobile() {
        findViewById(R.id.verify_mobile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.hideKeyboard(RegisterActivity.this);

                if (((TextInputEditText) findViewById(R.id.mobile)).getText().length() == 0) {
                    ((TextInputLayout) findViewById(R.id.mobile_box)).setError("Required");
                    return;
                } else {
                    ((TextInputLayout) findViewById(R.id.mobile_box)).setError("");
                }

                progressDialog = new ProgressDialog(RegisterActivity.this);
                progressDialog.setButton(ProgressDialog.BUTTON_POSITIVE, "ENTER CODE MANUALLY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                progressDialog.setMessage("Verifying...");
                progressDialog.show();

                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                        "+91" + (((TextInputEditText) findViewById(R.id.mobile)).getText().toString()),        // Phone number to verify
                        60,                 // Timeout duration
                        TimeUnit.SECONDS,   // Unit of timeout
                        RegisterActivity.this,               // Activity (for callback binding)
                        new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                            @Override
                            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                                progressDialog.dismiss();
                                linkMobileNumber(phoneAuthCredential);
                            }

                            @Override
                            public void onVerificationFailed(@NonNull FirebaseException e) {
                                progressDialog.dismiss();
                                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                                builder.setMessage("Mobile Verification Failed");
                                builder.show();
                            }

                            @Override
                            public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                                super.onCodeSent(s, forceResendingToken);
                            }

                            @Override
                            public void onCodeAutoRetrievalTimeOut(@NonNull String s) {
                                super.onCodeAutoRetrievalTimeOut(s);
                            }
                        });
            }
        });
        // OnVerificationStateChangedCallbacks
    }

    private void linkMobileNumber(PhoneAuthCredential credential) {
        mAuth.getCurrentUser().linkWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            findViewById(R.id.register).setEnabled(true);
                            mobileVerified = true;
                            findViewById(R.id.verify_mobile).setEnabled(false);
                            ((MaterialButton) findViewById(R.id.verify_mobile)).setText("VERIFIED");
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                            builder.setMessage("Mobile Verification Failed. ");
                            builder.show();
                        }

                        // ...
                    }
                });
    }

    private void register() {
        findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.hideKeyboard(RegisterActivity.this);
                boolean isValid = true;

                if (((TextInputEditText) findViewById(R.id.name)).getText().length() == 0) {
                    ((TextInputLayout) findViewById(R.id.name_box)).setError("Required");
                    isValid = false;
                }
                if (((TextInputEditText) findViewById(R.id.mobile)).getText().length() == 0) {
                    ((TextInputLayout) findViewById(R.id.mobile_box)).setError("Required");
                    isValid = false;
                }
                if (isValid) {
                    RadioGroup radioGroup = findViewById(R.id.occupation);
                    RadioButton radioButton = findViewById(radioGroup.getCheckedRadioButtonId());
                    String occupation = radioButton.getText().toString();
                    CheckBox checkBox = findViewById(R.id.experienced);
                    User user = new User(
                            mAuth.getCurrentUser().getUid(),
                            ((TextInputEditText) findViewById(R.id.name)).getText().toString(),
                            ((TextInputEditText) findViewById(R.id.mobile)).getText().toString(),
                            mAuth.getCurrentUser().getEmail(),
                            occupation,
                            "Rs 10000",
                            checkBox.isChecked(),
                            Helper.currentDateTime(),
                            0,
                            mAuth.getCurrentUser().getPhotoUrl().toString(),
                            mobileVerified
                    );
                    mDatabase.child("user").child(mAuth.getCurrentUser().getUid()).setValue(user)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(RegisterActivity.this, "REGISTERED", Toast.LENGTH_LONG).show();
                                        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            });
                }

            }
        });
    }

    private void preFillForm() {
        ((TextInputEditText) findViewById(R.id.name)).setText(mAuth.getCurrentUser().getDisplayName());
        ((TextInputEditText) findViewById(R.id.email)).setText(mAuth.getCurrentUser().getEmail());
    }

    private void initToolbar(String register) {
        ((TextView) findViewById(R.id.title)).setText(register);
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
