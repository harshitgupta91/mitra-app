package com.mitrafintech.associates.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.chip.Chip;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mitrafintech.associates.Application.Constant;
import com.mitrafintech.associates.Application.Helper;
import com.mitrafintech.associates.Database.Lead;
import com.mitrafintech.associates.Database.Product;
import com.mitrafintech.associates.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class ResultsActivity extends AppCompatActivity {

    private FirebaseAnalytics firebaseAnalytics;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private ProgressDialog progressDialog;
    private String leadId;
    private ProductsAdapter productsAdapter;
    private List<Product> productList;
    private boolean isBankVisible = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        leadId = getIntent().getStringExtra("leadid");

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        firebaseDatabase = Helper.getDatabase();
        mDatabase = firebaseDatabase.getReference(Constant.FIREBASE_DB);
        mAuth = FirebaseAuth.getInstance();
        initToolbar("Eligible Products");

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        productList = new ArrayList<>();
        mDatabase.child("product").orderByChild("rating").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progressDialog.dismiss();
                productList.clear();
                for (DataSnapshot snapshot :
                        dataSnapshot.getChildren()) {
                    Product product = snapshot.getValue(Product.class);
                    productList.add(product);
                }
                if (productList.size() == 0) {
                    Toast.makeText(ResultsActivity.this, "NO PRODUCTS FOUND", Toast.LENGTH_LONG).show();
                }
                filterProductData(productList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (!isBankVisible) {
            toggleBankVisibility(true);
        } else {
            super.onBackPressed();
        }
    }

    private void setupBankRecyclerView(final List<Product> productList1) {
        ListView lvBanks = findViewById(R.id.banks);
        HashSet<String> hashSet = new HashSet<>();
        for (Product product : productList1) {
            hashSet.add(product.getBank());
        }
        String[] banks = new String[hashSet.size()];
        banks = hashSet.toArray(banks);

        lvBanks.setAdapter(new BanksAdapter(banks, productList1));
    }

    private void toggleBankVisibility(boolean isVisible) {
        isBankVisible = isVisible;
        if (isVisible) {
            findViewById(R.id.banks).setVisibility(View.VISIBLE);
            findViewById(R.id.results).setVisibility(View.GONE);
        } else {
            findViewById(R.id.banks).setVisibility(View.GONE);
            findViewById(R.id.results).setVisibility(View.VISIBLE);
        }

    }

    private void setupProductRecyclerView(List<Product> productList1) {
        RecyclerView productsRecyclerView = findViewById(R.id.results);
        productsAdapter = new ProductsAdapter(productList1, this);
        productsRecyclerView.setAdapter(productsAdapter);
        productsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void filterProductData(final List<Product> productList) {
        Collections.reverse(productList);
        mDatabase.child("lead").child(leadId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<Product> productList1 = new ArrayList<>();
                Lead lead = dataSnapshot.getValue(Lead.class);
                for (Product product : productList) {
                    if (lead.getIncome() > product.getIncome()) {
                        productList1.add(product);
                    }
                }
                if (productList1.size() == 0) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(ResultsActivity.this);
                    dialog.setMessage("You are not qualified for any product");
                    dialog.show();
                }
                toggleBankVisibility(true);
                setupBankRecyclerView(productList1);
                setupProductRecyclerView(productList1);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void initToolbar(String register) {
        ((TextView) findViewById(R.id.title)).setText(register);
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void showCongratsPage() {
        findViewById(R.id.submitted).setVisibility(View.VISIBLE);
        findViewById(R.id.home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {


        private List<Product> productList;
        private Context context;

        public ProductsAdapter(List<Product> productList, Context context) {

            this.productList = productList;
            this.context = context;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View productView = inflater.inflate(R.layout.result_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(productView);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ProductsAdapter.ViewHolder holder, int position) {
            final Product product = productList.get(position);
            Glide.with(context).load(product.getImage()).into(holder.imgIcon);
            holder.tvProduct.setText(product.getProduct());
            holder.tvName.setText(product.getName());
            holder.tvBank.setText(product.getBank());
            holder.chipDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage(Html.fromHtml(product.getDescription()));
                    builder.show();
                }
            });
            holder.chipRating.setText(product.getCategory());
            holder.chipFees.setText("Fees : Rs" + product.getFees());
            holder.btnApply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<String, Object> map = new HashMap<>();
                    map.put("appliedProduct", product.getId());
                    mDatabase.child("lead").child(leadId).updateChildren(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                showCongratsPage();
                            }
                        }
                    });
                }
            });
        }

        @Override
        public int getItemCount() {
            return productList.size();
        }

        public void updateList(List<Product> productList) {
            this.productList = productList;
            notifyDataSetChanged();
        }


        private class ViewHolder extends RecyclerView.ViewHolder {

            TextView tvBank;
            TextView tvProduct, tvName;
            Chip chipFees, chipRating, chipDetail, btnApply;
            ImageView imgIcon;

            public ViewHolder(View itemView) {
                super(itemView);
                tvProduct = itemView.findViewById(R.id.product);
                tvBank = itemView.findViewById(R.id.bank);
                chipDetail = itemView.findViewById(R.id.detail);
                tvName = itemView.findViewById(R.id.name);
                chipFees = itemView.findViewById(R.id.fees);
                chipRating = itemView.findViewById(R.id.rating);
                btnApply = itemView.findViewById(R.id.apply);
                imgIcon = itemView.findViewById(R.id.icon);
            }
        }
    }

    private class BanksAdapter extends BaseAdapter {

        private String[] banks;
        private List<Product> productList;

        public BanksAdapter(String[] banks, List<Product> productList) {
            this.banks = banks;
            this.productList = productList;
        }

        @Override
        public int getCount() {
            return banks.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(ResultsActivity.this);
                view = inflater.inflate(R.layout.bank_item, parent, false);
            }

            TextView textView = view.findViewById(R.id.name);
            textView.setText(banks[position]);

            Chip chip = view.findViewById(R.id.count);
            int count = 0;
            Product product1 = new Product();
            for (Product product :
                    productList) {
                if (product.getBank().equals(banks[position])) {
                    count++;
                    product1 = product;
                }
            }
            chip.setText("" + count);
            ImageView imageView = view.findViewById(R.id.image);
            Glide.with(ResultsActivity.this).load(product1.getBankImage()).into(imageView);
            view.findViewById(R.id.layout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleBankVisibility(false);
                    List<Product> productList2 = new ArrayList<>();
                    for (Product product :
                            productList) {
                        if (product.getBank().equals(banks[position])) {
                            productList2.add(product);
                        }
                    }
                    productsAdapter.updateList(productList2);
                }
            });

            return view;
        }
    }

}
